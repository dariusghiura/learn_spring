package ro.codemart.learn_spring.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@Data
public class User {
        @Id
        private String id;
        @Column
        private String email;
        @Column
        private String password;
        @Column
        private String role;

        public User(){

        }

        public User(String id, String email, String password) {
                this.id = id;
                this.email = email;
                this.password = password;
        }

        public String getId() {
                return id;
        }

        public String getEmail() {
                return email;
        }

        public String getPassword() {
                return password;
        }

        public String getRole() {
                return role;
        }


        public void setEmail(String email) {
                this.email = email;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        @Override
        public String toString() {
                return "User{" +
                        "id='" + id + '\'' +
                        ", email='" + email + '\'' +
                        ", password='" + password + '\'' +
                        '}';
        }
}