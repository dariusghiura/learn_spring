package ro.codemart.learn_spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.codemart.learn_spring.entity.User;
import ro.codemart.learn_spring.service.UserService;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/user/all")
    public ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUsers());
    }

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity getUser(@PathVariable String userId) {
        User u =  userService.findById(userId);
        if (u != null){
            return ResponseEntity.status(HttpStatus.OK).body(u);
        }
        // If the user was not found return status 300
        return ResponseEntity.status(300).body(null);
    }

    @PostMapping(value = "/user/insert", consumes = "application/json", produces = "application/json")
    public User insertUser(@RequestBody User user){
        return userService.insertUser(user);
    }

    @DeleteMapping(value = "/user/delete/{userId}")
    public User deleteUser(@PathVariable String userId){
        User u = userService.deleteUser(userId);
        if (u != null){
            return u;
        }
        return new User();
    }

    @PutMapping(value = "/user/update", consumes = "application/json", produces = "application/json")
    public User updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

}
