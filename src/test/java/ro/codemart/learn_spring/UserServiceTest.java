package ro.codemart.learn_spring;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import ro.codemart.learn_spring.entity.User;
import ro.codemart.learn_spring.repository.UserRepository;

import java.util.List;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @MockBean
    UserRepository mockRepository;

    @Autowired
    ApplicationContext context;

    @Test
    public void mockBeanTest() {
        User u = new User("10", "mail10", "pass10");
        List<User> initialList = List.of(u);
        Mockito.when(mockRepository.findAll()).thenReturn(initialList);

        UserRepository userRepoFromContext = context.getBean(UserRepository.class);
        List<User> users = userRepoFromContext.findAll();

        Assert.assertEquals(users.size(), initialList.size());
        Mockito.verify(mockRepository).findAll();
    }
}
