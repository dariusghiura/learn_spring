package ro.codemart.learn_spring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ro.codemart.learn_spring.service.UserService;

@SpringBootTest
class LearnSpringApplicationTests {
    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        System.out.println(userService.getUsers());
        System.out.println(new BCryptPasswordEncoder().encode("pass1"));
    }

}
