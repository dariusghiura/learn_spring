package ro.codemart.learn_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.codemart.learn_spring.UserDetail;
import ro.codemart.learn_spring.entity.User;
import ro.codemart.learn_spring.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getUsers(){
        return (List<User>) userRepository.findAll();
    }

    public User findById(String id){
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.orElse(null);
    }

    public User insertUser(User user){
        return userRepository.save(user);
    }

    @Transactional
    public User deleteUser(String id){
        User u = findById(id);
        if (u != null) {
            userRepository.delete(u);
        }
        return u;
    }

    @Transactional
    public User updateUser(User user){
        User toEdit = findById(user.getId());
        if (toEdit != null){
            toEdit.setEmail(user.getEmail());
            toEdit.setPassword(user.getPassword());

            userRepository.save(toEdit);
        }
        return toEdit;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException(s);
        }
        return new UserDetail(user);
    }
}
