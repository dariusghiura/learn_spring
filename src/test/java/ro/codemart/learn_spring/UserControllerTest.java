package ro.codemart.learn_spring;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ro.codemart.learn_spring.controller.UserController;
import ro.codemart.learn_spring.entity.User;
import ro.codemart.learn_spring.service.UserService;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
//@WebMvcTest(UserController.class)
public class UserControllerTest {

//    @Autowired
//    private MockMvc mvc;
//    @MockBean
//    private UserService userService;
//
    private static final List<User> TEST_USER_LIST = createTestUserList();

    private static List<User> createTestUserList(){
        User u1 = new User("1", "mail1", "pass1");
        User u2 = new User("2", "mail2", "pass2");
        User u3 = new User("3", "mail3", "pass3");
        User u4 = new User("4", "mail4", "pass4");
        User u5 = new User("5", "mail5", "pass5");
        return List.of(u1, u2, u3, u4, u5);
    }

    @InjectMocks
    private UserController userController = new UserController();

    @Mock
    private UserService userService;

    @Autowired
    private MockMvc mvc;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        this.mvc= MockMvcBuilders.standaloneSetup(userController).build();
    }


    @Test
    public void getAllUsers() throws Exception {

        given(userService.getUsers()).willReturn(TEST_USER_LIST);

        mvc.perform(get("/user/all")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].email", is(TEST_USER_LIST.get(0).getEmail())));
    }

    @Test
    public void givenId_getUserWithId() throws Exception {
        User u = new User("3", "mail3", "pass3");
        given(userService.findById("3")).willReturn(u);

        mvc.perform(get("/user/3")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("@.email", is("mail3")));
    }

    @Test
    public void givenId_getUserWithWrongId() throws Exception {
        given(userService.findById("-1")).willReturn(null);

        mvc.perform(get("/user/-1")
                .contentType("application/json"))
                .andExpect(status().is(300));
    }
}
